<?php namespace FileTransfer;


use InvalidArgumentException;

class Factory
{
    /**
     * @param string $protocol Используемый протокол
     * @param string $server_url Путь до сервера
     * @param string $user_name Имя пользователя
     * @param string $password пароль
     * @param int|null $port порт
     * @return Connection
     */
    public static function getConnection(
        string $protocol,
        string $server_url,
        string $user_name,
        string $password,
        int $port = null) : Connection
    {
        if (!AllowedProtocols::isExists($protocol)) {
            throw new InvalidArgumentException("Protocol '$protocol' is not exists");
        }

        switch ($protocol) {
            case AllowedProtocols::SSH:
                return new SFTPConnection($server_url, $user_name, $password, $port);
            case AllowedProtocols::FTP:
                return new FTPConnection($server_url, $user_name, $password, $port);
        }
        return null;
    }
}