<?php namespace FileTransfer;


use InvalidArgumentException;

/**
 * Абстрактный класс соединения для операций с файлами на удаленном сервере
 * Class Connection
 * @package FileTransfer
 */
abstract class Connection
{

    /**
     * Идентификатор соединения
     * @var
     */
    private $connection_id;

    /**
     * Название протокола
     * @var string
     */
    private $connection_protocol;

    /**
     * Веб-адрес сервера
     * @var string
     */
    private $sever_url;

    /**
     * Имя пользователя
     * @var string
     */
    private $user_name;

    /**
     * пароль пользователя
     * @var string
     */
    private $password;

    /**
     * Порт сервера
     * @var int|null
     */
    private $port;

    /**
     * Рабочая директория соединения
     * @var string
     */
    private $current_directory;

    /**
     * Connection constructor.
     * @param string $protocol_name Название протокола
     * @param string $server_url Адрес файлового сервера
     * @param string $user_name Имя пользователя
     * @param string $password пароль пользователя
     * @param int|null $port Номер порта сервера
     */
    public function __construct(string $protocol_name, string $server_url, string $user_name, string $password, int $port = null)
    {
        if (AllowedProtocols::isExists($protocol_name)) {
            if (filter_var($server_url, FILTER_VALIDATE_URL) === FALSE) {
                if (filter_var($server_url, FILTER_VALIDATE_IP) === FALSE) {
                    throw new InvalidArgumentException("'$server_url' is not valid server url");
                }
            }

            $this->connection_protocol = $protocol_name;
            $this->sever_url = $server_url;
            $this->user_name = $user_name;
            $this->password = $password;
            $this->port = $port;
        } else {
            throw new InvalidArgumentException("Protocol '$protocol_name' is not exists");
        }
    }

    /**
     * Установить соединение с сервером
     * @param $connection_id
     * @throws InvalidArgumentException В случае если не удалось установить соединение
     */
    public function setId($connection_id)
    {
        if ($connection_id === false) {
            throw new InvalidArgumentException('Cant connect to sever');
        }

        $this->connection_id = $connection_id;
    }

    /**
     * Получить идентификатор соединения
     * @return mixed
     */
    public function getId()
    {
        return $this->connection_id;
    }

    /**
     * Получить название используемого протокола
     * @return string
     */
    public function getConnectionProtocol(): string
    {
        return $this->connection_protocol;
    }

    /**
     * Получть веб-адрес сервера
     * @return string
     */
    public function getSeverUrl(): string
    {
        return $this->sever_url;
    }

    /**
     * Получить имя пользователя
     * @return string
     */
    public function getUserName(): string
    {
        return $this->user_name;
    }

    /**
     * Получить пароль пользователя
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Получить порт сервера
     * @return int|null
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Авторизация на сервере
     * @param string $user_name Имя пользователя
     * @param string $password Пароль
     * @return bool
     */
    abstract function login(string $user_name, string $password): bool;


    /**
     * Скачивание файла
     * @param string $server_file_name
     * @param string $local_file
     * @return Connection
     */
    abstract function download(string $server_file_name, string $local_file) : Connection;

    /**
     * Загрузка файла на сервер
     * @param string $server_file_name
     * @param string $local_file
     * @return Connection
     */
    abstract function upload(string $server_file_name, string $local_file) : Connection;


}