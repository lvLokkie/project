<?php namespace FileTransfer;


use ReflectionClass;

class AllowedProtocols
{
    const FTP = 'ftp';
    const SSH = 'ssh';

    /**
     * Проверка на существование протокола
     * @param string $protocol_name Наименование протокола
     * @return bool
     */
    public static function isExists(string $protocol_name): bool
    {
        $class = new ReflectionClass(__CLASS__);
        $constants = $class->getConstants();
        return key_exists(strtoupper($protocol_name), $constants);
    }
}