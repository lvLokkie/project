<?php
/**
 * Created by PhpStorm.
 * User: RaLf_
 * Date: 11.01.2017
 * Time: 20:46
 */

namespace FileTransfer;


use Exception;
use InvalidArgumentException;

class SFTPConnection extends Connection
{
    const DEFAULT_SFTP_PORT = 22;
    const FILE_READ_MODE = 'r';
    const FILE_WRITE_MODE = 'w';

    private $sftp_connection;

    public function __construct($server_url, $user_name, $password, $port = null)
    {
        if (!function_exists('ssh2_connect')) {
            throw new Exception('Function ssh2_connect does not exist. Try to check libssh2-php extension');
        }

        parent::__construct(
            AllowedProtocols::SSH,
            $server_url,
            $user_name,
            $password,
            $port
        );

        $this->setId(
            ssh2_connect(
                $server_url,
                $port ?? self::DEFAULT_SFTP_PORT
            )
        );


        if (!$this->login($user_name, $password)) {
            throw new InvalidArgumentException('Bad login or password');
        }

        $this->sftp_connection = ssh2_sftp($this->getId());
    }

    /**
     * Авторизовать соединение
     * @param string $user_name Имя пользователя
     * @param string $password пароль
     * @return bool
     */
    public function login(string $user_name, string $password): bool
    {
        return ssh2_auth_password($this->getId(), $user_name, $password);
    }

    /**
     * Получить полный адрес файла на сервере
     * @param $file_path string путь к файлу
     * @return string
     */
    private function getSFTPFilePath(string $file_path):string
    {
        return 'ssh2.sftp://' . $this->sftp_connection . $file_path;
    }

    /**
     * Скачивание файла
     * @param string $server_file_path Путь к удаленному файлу
     * @param string $local_file_path Путь к локальному файлу
     * @return Connection
     * @throws Exception В случае неудачи скачичвания
     */
    public function download(string $server_file_path, string $local_file_path = '/var/www/downloaded') : Connection
    {
        $sftp_file_path = $this->getSFTPFilePath($server_file_path);

        if (!$remote = fopen($sftp_file_path, self::FILE_READ_MODE)) {
            throw new Exception("Failed to open remote file: $server_file_path");
        }

        if (!$local = fopen($local_file_path, self::FILE_WRITE_MODE)) {
            throw new Exception("Failed to create local file: $local_file_path");
        }

        $read = 0;
        $file_size = filesize($sftp_file_path);

        while (($read < $file_size) && ($buffer = fread($remote, $file_size - $read))) {

            $read += strlen($buffer);

            if (fwrite($local, $buffer) === FALSE) {
                throw  new Exception("Failed to write to local file: $local_file_path");
                break;
            }
        }

        fclose($local);
        fclose($remote);

        return $this;
    }

    /**
     * Загрузка файла на сервер
     * @param string $server_file_path Путь к удаленному файлу
     * @param string $local_file_path Путь к локальному файлу
     * @return Connection
     * @throws Exception В случае неудачи
     */
    public function upload(string $server_file_path, string $local_file_path = '/var/www/uploaded') : Connection
    {
        $sftp_file_path = $this->getSFTPFilePath($server_file_path);

        if (!$remote = fopen($sftp_file_path, self::FILE_WRITE_MODE)) {
            throw new Exception("Failed to create remote file: $server_file_path");
        }

        if (!$local = fopen($local_file_path, self::FILE_READ_MODE)) {
            throw new Exception("Failed to open local file: $local_file_path");
        }

        $file = file_get_contents($local_file_path);
        fwrite($remote, $file);
        fclose($remote);

        return $this;
    }

    /**
     * Изменить рабочую директорию
     * @param string $directory
     * @return $this
     */
    public function cd(string $directory)
    {
        if (is_dir($directory)) {
            ssh2_exec($this->sftp_connection, "cd $directory");
            return $this;
        } else {
            throw new InvalidArgumentException("'$directory' is not valid directory");
        }
    }

    /**
     * Завершить соединение
     * @return  bool
     */
    public function close()
    {
        return ssh2_exec($this->sftp_connection, 'exit');
    }


}