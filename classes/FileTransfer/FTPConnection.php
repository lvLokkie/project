<?php namespace FileTransfer;


use Exception;
use InvalidArgumentException;

class FTPConnection extends Connection
{
    const DEFAULT_FTP_PORT = 21;

    public function __construct($server_url, $user_name, $password, $port = null)
    {
        parent::__construct(
            AllowedProtocols::FTP,
            $server_url,
            $user_name,
            $password,
            $port
        );

        $this->setId(
            ftp_connect(
                $server_url,
                $port ?? self::DEFAULT_FTP_PORT
            )
        );

        if (!$this->login($user_name, $password)) {
            throw new InvalidArgumentException('Bad login or password');
        } else {
            ftp_pasv($this->getId(), true);
        }

    }

    public function login(string $user_name, string $password): bool
    {
        return ftp_login($this->getId(), $user_name, $password);
    }

    /**
     * Получаем корректный способ передачи данных для файла
     * @param $file_name string Название файла
     * @return int
     */
    private function getFTPMode(string $file_name)
    {
        $path_parts = pathinfo($file_name);
        $file_extension = $path_parts['extension'];

        if (!isset($file_extension))
                return FTP_BINARY;

        switch (strtolower($file_extension)) {
            case 'am':case 'asp':case 'bat':case 'c':case 'cfm':case 'cgi':case 'conf':
            case 'cpp':case 'css':case 'dhtml':case 'diz':case 'h':case 'hpp':case 'htm':
            case 'html':case 'in':case 'inc':case 'js':case 'm4':case 'mak':case 'nfs':
            case 'nsi':case 'pas':case 'patch':case 'php':case 'php3':case 'php4':case 'php5':
            case 'phtml':case 'pl':case 'po':case 'py':case 'qmail':case 'sh':case 'shtml':
            case 'sql':case 'tcl':case 'tpl':case 'txt':case 'vbs':case 'xml':case 'xrc':
                return FTP_ASCII;

            default:
                return FTP_BINARY;
        }

    }

    /**
     * Скачивание файла
     * @param string $server_file_path
     * @param string $local_file_path
     * @return Connection
     * @throws Exception В случае неудачи скачания
     */
    public function download(string $server_file_path, string $local_file_path = '/var/www/downloaded') : Connection
    {
        $is_successful = ftp_get(
            $this->getId(),
            $local_file_path,
            $server_file_path,
            $this->getFTPMode($server_file_path)
        );

        if (!$is_successful) {
            throw  new Exception('Error while file is downloading');
        }

        return $this;
    }

    /**
     * Загрузка файла на сервер
     * @param string $server_file_path
     * @param string $local_file_path
     * @return Connection
     * @throws Exception В случае неудачи скачания
     */
    public function upload(string $server_file_path, string $local_file_path = '/var/www/uploaded') : Connection
    {
        $is_successful = ftp_put(
            $this->getId(),
            $server_file_path,
            $local_file_path,
            $this->getFTPMode($server_file_path)
        );

        if (!$is_successful) {
            throw  new Exception('Error while file is downloading');
        }

        return $this;
    }

    /**
     * Изменить рабочую директорию соединения
     * @param string $directory
     * @throws InvalidArgumentException В случае, если аргумент не является директорией
     * @return Connection
     */
    public function cd(string $directory):Connection
    {
        if (is_dir($directory)) {
            ftp_chdir($this->getId(), $directory);
        } else {
            throw  new InvalidArgumentException("'$directory' is not directory");
        }

        return $this;
    }

    /**
     * Получить рабочую директорию соединения
     * @return string Текущая рабочая директория
     * @throws Exception В случае ошибки получения рабочей директории
     */
    public function pwd(): string
    {
        $pwd = ftp_pwd($this->getId());
        if ($pwd === false) {
            throw new Exception('Cant get current working directory');
        }

        return $pwd;
    }

    /**
     * Выполнить команду на удаленном сервере
     * @param string $command Команда
     * @return array
     */
    public function exec(string $command): array
    {
        return ftp_raw($this->getId(), $command);
    }
}