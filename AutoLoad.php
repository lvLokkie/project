<?php namespace  common;

//Автозагрузчик
class AutoLoad
{
    // Карта для соответствия неймспейса пути в корневой папке проекта проекте
    protected $namespacesMap
        = array(
            'common' => '/',
            'FileTransfer' => '/classes/FileTransfer/'
        );

    /**
     * AutoLoader constructor.
     * @param array | null $namespaces Массив соотвествий пространств имен и физических путей в проекте
     */
    public function __construct($namespaces = null)
    {
        if (isset( $namespaces ) && is_array($namespaces))
        {
            foreach ($namespaces as $namespace)
            {
                if (!key_exists($namespace, $namespaces))
                {
                    array_merge($namespace, array(key($namespace) => $namespace));
                };
            }
        };
    }

    /**
     * Регистрирует данный класс как автоматический загрузчик файлов-скриптов
     */
    public function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }


    /**
     * Функция автозагрузки файла
     * @param $class string Имя файла
     * @return bool Удалось ли подключить файл
     */
    protected function autoload($class)
    {
        $pathParts = explode('\\', $class);
        if (is_array($pathParts) && count($pathParts) > 0)
        {
            $namespace = array_shift($pathParts);
            $count = count($pathParts);
            //Оставляем только имя файла
            while ($count > 1)
            {
                $namespace = $namespace .'\\'. array_shift($pathParts);
                $count = count($pathParts);
            }
            if (!empty( $this->namespacesMap[$namespace] ))
            {
                $filePath = __DIR__ . $this->namespacesMap[$namespace] . array_shift($pathParts) . '.php';
                require_once $filePath;
                return true;
            }
        }
        return false;
    }


}