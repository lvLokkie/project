<?php

//Подключаем автозагрузчик
require_once('AutoLoad.php');
$loader = new common\AutoLoad();
$loader->register();


use FileTransfer as FT;


$factory = new FT\Factory();
try
{
    /** @var FT\SFTPConnection $conn */
    $conn = $factory->getConnection('ssh', '127.0.0.1', 'test', 'test');
    $conn->cd('/var/www')
        ->download('dump.tar.gz')
        ->close();
}
catch(Exception $e)
{
    echo $e->getMessage();
}

try
{
    /** @var FT\FTPConnection $conn */
    $conn = $factory->getConnection('ftp', '127.0.0.1',  'test', 'test');
    echo $conn->pwd() . "\n";
    $conn->upload('archive.zip');
    print_r($conn->exec('ls -al'));
}
catch (Exception $e)
{
    echo $e->getMessage();
}